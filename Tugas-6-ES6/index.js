
//soal 1
let KelilingdanLuasPersegiPanjang = (panjang, lebar) => {
    Keliling_Persegi_Panjang = panjang*2 + lebar*2
    Luas_Persegi_Panjang = panjang*lebar
    return 'Keliling : '+ Keliling_Persegi_Panjang + ' Luas : ' + Luas_Persegi_Panjang
}; console. log(KelilingdanLuasPersegiPanjang(3,4))


//soal 2
const Fullname = (firstname,lastname) => firstname + ' ' + lastname
; console.log(Fullname ("William", "Imoh"));


//soal 3
let data = {
    firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};
 const {firstName, lastName, address, hobby} = data
 console.log (firstName, lastName, address, hobby);


//soal 4
let west = ['Will', 'Chris', 'Sam', 'Holly']
let east = ['Gill', 'Brian', 'Noel', 'Maggie']
let combine = [...west, ...east]
console.log(combine)


//soal 5
const planet = "earth" 
const view = "glass" 
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log (before)